(defpackage SISA
  (:import-from :str :substring :concat :s-rest :s-first)
  (:use :cl))
(in-package :SISA)
(require 'unix-opts)
(require 'str)

(defun debug-ignore (c h) (declare (ignore h)) (print c) (abort))
(setf *debugger-hook* #'debug-ignore)

(defun substring-to-int (start end string base)
  (parse-integer (str:substring start end string) :radix base))

(defun fix-to-base (input in-base out-base len)
  (let* ((binary-string (write-to-string (parse-integer input :radix in-base) :base out-base))
	 (prefix (make-string (- len (length binary-string)) :initial-element #\0)))
    (str:concat prefix binary-string)))

(defun fix-negatives (string-int max)
  (write-to-string (- max (parse-integer (str:s-rest string-int)))))

(defun sisa-alu (input)
  (let* ((ra (substring-to-int 4 7 input 2))
	 (rb (substring-to-int 7 10 input 2))
	 (rd (str:substring 10 13 input))
	 (f (substring-to-int 13 16 input 2)))
    (if (string= "XXX" rd)
	(setq rd "-")
	(setq rd (parse-integer rd :radix 2)))
    (if (and (string= (str:substring 3 4 input) "1") (or (= f 2) (> f 5)))
	(format t "~&Invalid order ~%")
	(format t "~&~a R~d, R~d, R~d~%"
		(nth f (nth (substring-to-int 3 4 input 2)
			    (list
			     (list "AND" "OR" "XOR" "NOT" "ADD" "SUB" "SHA" "SHL")
			     (list "CMPLT" "CMPLE" "INVALID
		OP" "CMPEQ" "CMPLTU" "CMPLEU"))))
		rd ra rb))))

(defun sisa-addi (input)
  (let* ((ra (substring-to-int 4 7 input 2))
	 (rd (str:substring 7 10 input))
	 (n (substring-to-int 11 16 input 2)))
    (when (string= (str:substring 10 11 input) "1")
      (setq n (- n 32)))
    (if (string= "XXX" rd)
	(setq rd "-")
	(setq rd (parse-integer rd :radix 2)))
    (format t "~&ADDI R~d, R~d, ~d~%"
	    rd ra n)))

(defun sisa-ld (input)
  (let* ((ra (substring-to-int 4 7 input 2))
	 (rd (substring-to-int 7 10 input 2))
	 (n (substring-to-int 11 16 input 2)))
    (when (string= (str:substring 10 11 input) "1")
      (setq n (- n 32)))
    (format t "~&~a R~d, ~d(R~d)~%"
	    (nth (substring-to-int 2 3 input 2) (list "LDB" "LD"))
	    rd n ra)))

(defun sisa-st (input)
  (let* ((ra (substring-to-int 4 7 input 2))
	 (rb (substring-to-int 7 10 input 2))
	 (n (substring-to-int 11 16 input 2)))
    (when (string= (str:substring 10 11 input) "1")
      (setq n (- n 32)))
    (format t "~&~a ~d(R~d), R~a~%"
	    (nth (substring-to-int 2 3 input 2) (list "ST" "STB"))
	    n ra rb)))

(defun sisa-jalr (input)
  (let* ((ra (substring-to-int 4 7 input 2))
	 (rb (substring-to-int 7 10 input 2)))
    (format t "~&JALR R~d, R~d~%"
	    ra rb)))

(defun sisa-b (input)
  (let* ((ra (substring-to-int 4 7 input 2))
	 (n (substring-to-int 9 16 input 2)))
    (when (string= (str:substring 8 9 input) "1")
      (setq n (- n 128)))
    (format t "~&~a R~d, ~a~%"
	    (nth (substring-to-int 7 8 input 2) (list "BZ" "BNZ"))
	    ra n)))

(defun sisa-mov (input)
  (let* ((rd (substring-to-int 4 7 input 2))
	 (n (substring-to-int 9 16 input 2)))
    (when (string= (str:substring 8 9 input) "1")
      (setq n (- n 128)))
    (format t "~&~a R~d, ~a~%"
	    (nth (substring-to-int 7 8 input 2) (list "MOVI" "MOVHI"))
	    rd n)))

(defun sisa-in-out (input)
  (let* ((r (substring-to-int 4 7 input 2))
	 (n (substring-to-int 8 16 input 2)))
    (if (= 0 (substring-to-int 7 8 input 2))
	(format t "~&IN R~a, ~d ~%" r n)
	(format t "~&OUT ~d, R~a ~%" n r))))

(defun from-mnemo (mnemo)
  (cond ((find (first mnemo) '("IN" "OUT") :test 'equal)
	 (format NIL "~&1010~a~a~a"
		 (fix-to-base (str:s-rest (nth (+ 1 (position (first mnemo) '("IN" "OUT") :test 'equal)) mnemo)) 10 2 3)
		 (position (first mnemo) '("IN" "OUT") :test 'equal)
		 (fix-to-base (nth (- 2 (position (first mnemo) '("IN" "OUT") :test 'equal)) mnemo) 10 2 8)))
	((find (first mnemo) '("MOVI" "MOVHI") :test 'equal)
	 (if (string= "-" (str:s-first (third mnemo)))
	     (format NIL "~&1001~a~a1~a"
		     (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		     (position (first mnemo) '("MOVI" "MOVHI") :test 'equal)
		     (fix-to-base (fix-negatives (third mnemo) 128) 10 2 7))
	     (format NIL "~&1001~a~a0~a"
		     (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		     (position (first mnemo) '("MOVI" "MOVHI") :test 'equal)
		     (fix-to-base (third mnemo) 10 2 7))))
	((find (first mnemo) '("BZ" "BNZ") :test 'equal)
	 (if (string= "-" (str:s-first (third mnemo)))
	     (format NIL "~&1000~a~a1~a"
		     (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		     (position (first mnemo) '("BZ" "BNZ") :test 'equal)
		     (fix-to-base (fix-negatives (third mnemo) 128) 10 2 7))
	     (format NIL "~&1000~a~a0~a"
		     (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		     (position (first mnemo) '("BZ" "BNZ") :test 'equal)
		     (fix-to-base (third mnemo) 10 2 7))))
	((find (first mnemo) '("LD" "ST" "LDB" "STB") :test 'equal)
	 (if (string= "-" (str:s-first (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)))
	     (format NIL "~&0~a~a~a1~a"
		     (fix-to-base (write-to-string (+ 3 (position (first mnemo) '("LD" "ST" "LDB" "STB") :test 'equal))) 10 2 3)
		     (fix-to-base (str:substring (- (length (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 2) (- (length (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 1) (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 10 2 3)
		     (fix-to-base (str:s-rest (nth (- 2 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 10 2 3)
		     (fix-to-base (fix-negatives (str:substring 0 (- (length (nth (+ 1 (mod (position (first mnemo) (list "ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 4) (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 32) 10 2 5))
	     (format NIL "~&0~a~a~a0~a"
		     (fix-to-base (write-to-string (+ 3 (position (first mnemo) '("LD" "ST" "LDB" "STB") :test 'equal))) 10 2 3)
		     (fix-to-base (str:substring (- (length (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 2) (- (length (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 1) (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 10 2 3)
		     (fix-to-base (str:s-rest (nth (- 2 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 10 2 3)
		     (fix-to-base (str:substring 0 (- (length (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 4) (nth (+ 1 (mod (position (first mnemo) '("ST" "LD" "STB" "LDB") :test 'equal) 2)) mnemo)) 10 2 5))))
	((string= "JALR" (first mnemo))
	 (format NIL "~&0111~a~a000000"
		 (fix-to-base (str:s-rest (third mnemo)) 10 2 3)
		 (fix-to-base (str:s-rest (second mnemo)) 10 2 3)))
	((string= "ADDI" (first mnemo))
	 (if (string= "-" (str:s-first (fourth mnemo)))
	     (format NIL "~&0010~a~a1~a"
		     (fix-to-base (str:s-rest (third mnemo)) 10 2 3)
		     (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		     (fix-to-base (fix-negatives (fourth mnemo) 32) 10 2 5))
	     (format NIL "~&0010~a~a0~a"
		     (fix-to-base (str:s-rest (third mnemo)) 10 2 3)
		     (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		     (fix-to-base (fourth mnemo) 10 2 5))))
	((find (first mnemo) '("CMPLT" "CMPLE" "." "CMPEQ" "CMPLTU" "CMPLEU" "AND" "OR" "XOR" "NOT" "ADD" "SUB" "SHA" "SHL") :test 'equal)
	 (format NIL "~&000~a~a~a~a~a"
		 (mod (position (first mnemo) '("AND" "CMPLT" "OR" "CMPLE" "XOR" "CMPEQ" "NOT" "CMPLTU" "ADD" "CMPLEU" "SUB" "." "SHA" "." "SHL") :test 'equal) 2)
		 (fix-to-base (str:s-rest (third mnemo)) 10 2 3)
		 (fix-to-base (str:s-rest (fourth mnemo)) 10 2 3)
		 (fix-to-base (str:s-rest (second mnemo)) 10 2 3)
		 (fix-to-base (write-to-string (position (first mnemo) (nth (mod (position (first mnemo) '("AND" "CMPLT" "OR" "CMPLE" "XOR" "CMPEQ" "NOT" "CMPLTU" "ADD" "CMPLEU" "SUB" "." "SHA" "." "SHL") :test 'equal) 2) (list (list "AND" "OR" "XOR" "NOT" "ADD" "SUB" "SHA" "SHL") (list "CMPLT" "CMPLE" "." "CMPEQ" "CMPLTU" "CMPLEU"))) :test 'equal)) 10 2 3)))
	(t (format NIL "Not found."))))

(defun main (argv)
  (let* ((opcode NIL)
	 (binary-string NIL))
    (if (not (>= (length argv) 3))
	(format t "~&Usage: sisa -hex [XXXX] | -bin [XXXXXXXXXXXXXXXX] | -m M0 M1 ... Mn~%")
	(progn
	  (cond ((string= "-hex" (second argv))
		 (setq opcode (substring-to-int 0 1 (third argv) 16)
		       binary-string (fix-to-base (third argv) 16 2 16)))
		((string= "-bin" (second argv))
		 (setq opcode (substring-to-int 0 4 (third argv) 2)
		       binary-string (third argv)))
		((string= "-m" (second argv))
		 (setq opcode T
		       binary-string (from-mnemo (cddr argv)))))
	  (if (not opcode)
	      (format t "~&Usage: sisa -hex [XXXX] | -bin [XXXXXXXXXXXXXXXX]~%")
	      (if (string= "-m" (second argv))
		  (format t "~&~a | 0x~a~%"
			  binary-string
			  (fix-to-base binary-string 2 16 4))
		  (funcall (nth opcode 
				'(sisa-alu sisa-alu sisa-addi
				  sisa-ld sisa-st sisa-ld sisa-st sisa-jalr
				  sisa-b sisa-mov sisa-in-out))
			   binary-string)))))))
